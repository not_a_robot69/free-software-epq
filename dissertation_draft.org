#+title: Should Software be Free?
#+author: Hashim Jastaniah

* Introduction

With computer technology becoming an ever more prominent part of people's
daily lives, it is important for computer users to be aware of the software
they use, and what conditions it imposes on them. A lot of the popular
software today imposes many restrictions on users and restricts their
freedom; but how exactly does it do that? This paper will discuss the
difference between freedom-respecting software and proprietary software, and
whether software should be free or not.

* What is free software?

First and foremost, the 'free' in 'free software' refers to the liberty and
freedom of users. This is why free software is often times called libre
software, to avoid confusion. There is no set definition for free software,
except that it respects users' freedom. The most widely accepted definition
for what that entails is that for a program to be considered 'free', it needs
to give its users the /four essential freedoms/, set by the Free Software
Foundation (FSF). They are: 

0. [@0] The freedom to run the program as you wish, for any purpose.
1. [@1] The freedom to study how the program works, and change it so it does your computing as you wish. Access to the source code is a precondition for this.
2. [@2] The freedom to redistribute copies so you can help others
3. [@3] The freedom to distribute copies of your modified versions to others. By doing this, you can give the whole community a chance to benefit from your changes. Access to the source code is a precondition for this.

If a program doesn't meet these conditions for freedom, it is deemed
proprietary or non-free. (GNU. (2019). What is free software?. [online]
Available at: https://www.gnu.org/philosophy/free-sw.html.)

** Open source?

While 'free software' and 'open source software' refer to almost the same
range of programs, but they stand for completely different values. Free
software is an ethical issue and campaigns for the freedom for the users of
computing, whereas the open source ideology prefers open software for the
*practical* advantage of it. Not all open software respects the four
freedoms. This means that all free software is open source, but not all open
software is free. (GNU. (2010). Why Open Source Misses the Point of Free
Software [online] Available at:
https://www.gnu.org/philosophy/open-source-misses-the-point.html.)

* What is proprietary software?

Proprietary software is software that does not respect users' freedom (GNU.
(2021). Proprietary Software [online] Available at:
https://www.gnu.org/proprietary/proprietary.html). It usually doesn't conform
to any of the four essential freedoms, but some non-free open-source software
conforms to some of the freedoms.

** How proprietary software restricts users' freedom

Making software proprietary heavily restricts users' control over their
software. Proprietary software does this by imposing restrictive licenses
that prevent users from running, copying and distributing the program as they
wish, as well as preventing them from studying how it works, learning its
true function and changing it to suit their needs (GNU. (2020). Free Software
Is Even More Important Now. [online] Available at:
https://www.gnu.org/philosophy/free-software-even-more-important.html). Free
software, on the other hand, makes users control the program since they have
the freedom to run, copy, distribute, study, change and improve the software
as they wish. While one may not be a programmer themselves, and are
incapable of directly changing the program to suit their needs, changing the
program can be done as a community, as is often seen in the free software
community, with non-programmers giving suggestions and contributing in other
ways, such as assets (GNU. (2019). What is free software?. [online] Available
at: https://www.gnu.org/philosophy/free-sw.html.).

** Do you really own your hardware?

Without the freedom to run whatever program one wants on their machine, do
they really own their machine? For example, many peoples' iPhones and Android
phones have ARM processors, which would be fully capable of running an entire
desktop OS with the right software, but they are not allowed to do so because
the bootloader is locked, or only limited to running certain programs, such
as different Android ROMs. Even then, not many phones provide unlockable
bootloader for different Android ROMs. Many companies can use this to their
advantage and use planned obsolesence to make these devices unable to
function effectively after a certain period of time, despite the hardware
being perfectly capable of that, because they have control over one's
software. Despite buying that machine with your own money and 'owning' it,
you can't do what **you** want with it. Does that not seem unjust?

** Why would one choose proprietary software

People claim that there are reasons for choosing to develop and/or use
proprietary software. Here are some of them.

*** Profit

One of the biggest reasons for the development of proprietary software is to
profit from it, as there is nobody else who can offer the same service as the
proprietary software. That isn't to say it isn't possible to profit from free
software: software can be given free of charge and offer services such as
customer support for money. Many free projects also rely on donations for
profit (GNU. (2014). The GNU Manifesto. [online] Available at:
https://www.gnu.org/gnu/manifesto.html.). However, donations aren't as
reliable as a fixed price, and a lot of software requires little to no
technical support.

*** Security by obscurity

Some people may claim that proprietary software can be more secure, as nobody
can find exploits as the source code is unavailable. This, however, is a weak
argument, as security by obscurity was rejected by security experts as far
back as 1851 (Wikipedia. (2021). Security through obscurity. [online]
Available at: https://en.wikipedia.org/wiki/Security_through_obscurity
[Accessed 6 Apr. 2021]). Free software actually tends to be more secure: if
everyone can inspect the source code, then they can all improve it and patch
vulnerabilities, making the program more secure.

*** Competitive edge

Some people claim that proprietary software give a competitive edge:
competitors can't compete if they don't know how your software works. This
however, is very similar to security by obscurity above, so this argument is
just as weak. Even if one couldn't gain a competitive edge against their
competitors, neither will their competitors get an edge over them (GNU.
(2014). The GNU Manifesto. [online] Available at:
https://www.gnu.org/gnu/manifesto.html.).

* Software vs Information

If software should be free, then that poses the question: should information
(like books) also be free? While some people would also argue that knowledge
should be free, that is a different issue with different with different
reasons, known as the free culture movement, which can be discussed in a
different paper. While the two issues are similar and linked, creative works
and software are fundementally different. Books are a means to store and
convey information, while software is more similar to a tool that enables
someone to achieve a certain task. One of the major reasons that it is argued
that software should be free is because non-free software allows the 'owner'
of a piece software to exert power over the software's users, as they are not
in complete control of it. Software is also put in a position that naturally
has a lot more power than a creative work: it has access to users' data,
which enables it to do a lot more than just be a tool to achieve something.

* Copyright and Copyleft (might need to take out this section)

** What is copyright?

Copyright is a law that protects the use of one's work if it is /original/
and /tangible/ In order for it to be original, it needs to be the work of
one's own skill and labour/intellectual creation; and for it to be tangible,
it has to be expressed in a physical form, and not just an idea in one's
imagination (BBC. (2019). What is Copyright? [online] Available at:
https://www.bbc.co.uk/copyrightaware/what-is-copyright).

** What is copyleft?

Copyleft is a copyright license that, in the specific case of software, makes
the program free, and requires all modified versions of the program to be
licensed under the same license (GNU. (2009). What is Copyleft?. [online]
Available at: https://www.gnu.org/copyleft/.).

*** Why copyleft?

Putting free software in the public domain (no license) means that people can
change and/or improve the software, and only share their changes *if they
want to*. This means that somebody can convert that software to proprietary
software, and claim it as their own. In this way, copyleft guarantees users'
freedom (GNU. (2009). What is Copyleft?. [online] Available at:
https://www.gnu.org/copyleft/.).

* Conclusion

(just several notes to potential conclusions)
Freedom is a basic human right everyone deserves, and with technology present
in pretty much everywhere in people's lives, users must be free to control
the software they use, by being able to run, copy, distribute, study, change
and improve the software as they wish.
From a business perspective, it is better to create proprietary software to
maximize profit and to gain a competitive edge: but that doesn't address any
ethical issues.
You could say it isn't unethical to make and let people use proprietary
software, since it is their choice to agree to the license that the software
proposes. But what if all software was proprietary?
Each type of software has its audience and their reasons, therefore one can't
conclude that one party is 'right' or 'wrong'. We could say that since
proprietary software usually makes a lot of money, then it is the duty of
companies developing proprietary software to give money to organizations and
groups developing free software to support the learning society free software
promotes.
