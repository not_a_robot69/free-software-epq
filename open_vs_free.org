#+title: The difference between open-source software and free software
#+author: Hashim Jastaniah

'Free software' and 'open source software' refer to almost the same
range of programs. However, they stand for completely different
values. The idea of free software campaigns for the freedom for the
users of computing, while the open-source idea prefers open software
for its *practical* advantage. When software is 'free', it respects
the users' essential freedoms (see 'free_meaning.org/html').
