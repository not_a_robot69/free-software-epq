#+title: Harvord Referencing
#+author: Hashim Jastaniah

** Written in this order:
1. Name of the author(s)
2. Year published
3. Title
4. City published
5. Publisher
6. Pages used

E.g: Last name, First Initial. (Year published). Title. City: Publisher, Page(s).

*** Structure for online articles: 
Last name, First initial. (Year published). Article Title. Journal, [online] Volume(Issue), pages. Available at: URL [Accessed Day Mo. Year].

*** Structure for newspaper articles:
Last name, First initial. (Year published). Article title. Newspaper, Page(s).
(online newspaper)
Last name, First initial. (Year published). Article title. Newspaper, [online] pages. Available at: url [Accessed Day Mo. Year].

*** Structure for magazines:
Last name, First initial. (Year published). Article title. Magazine, (Volume), Page(s).

*** Structure for eBooks and PDFs:
Last name, First initial. (Year published). Title. Edition. [format] City: Publisher, page(s). Available at: URL [Accessed Day Mo. Year]. 
