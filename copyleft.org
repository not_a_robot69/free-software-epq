#+title: What is copyleft?

Copyleft is a form of copyright that (in software) makes a program free and
requires all modified versions of the program free software as well. The reason
it exists is because if one puts their software in the public domain (not
copyrighted, and without any 'restrictions'), it allows people to change the
software, improve it and share it, if they want to. But it also allows
incooperative people to convert the program into proprietary software, by making
few or many changes, and redistributing however they want, since the original
product has no licensing. Proprietary software developers use copyright to take
away users' freedom; copyleft uses copyright to guarantee users' freedom; this
is why the name is reversed, changing the 'right' to 'left'. Copyleft is a
general concept; there are many ways to implement it. For example, the GNU
General Public License (GNU GPL) and the Lesser GPL (LGPL).

*** Sources

- https://www.gnu.org/copyleft/
- (not used, but interesting and related) https://www.gnu.org/philosophy/pragmatic.html
