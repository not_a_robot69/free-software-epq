#+title: What is proprietary software?
#+author: Hashim Jastaniah

Proprietary software (or non-free or closed-source software) is software that
does not respect its users' freedom and community (see free_meaning.org/html for
what exactly software that respects its users' freedom entails). Since the users
cannot see nor modify the program to their needs, the developer of the
prorietary program has control over the users as they can change whatever they
want without approval of the users. For example, a lot of proprietary programs
collect personal data about its users, or force them to use functionality they
do not like. This functionality can often times be malicious. Many proprietary
programs also restrict its users from doing certain things, and censors
information they do not want its users to see. This means that the developer has
control over the users, making the proprietary program an instrument of injust
power. Many programs (including, but not limited to, Windows, mobile phone
firmware, and Google Chrome for Windows) include a universal backdoor which
allows remote access to the machnine running the software without the consent of
its users. The Amazon Kindle has a backdoor which allows it to erase books (as
they did when they deleted copies of '1984' in 2009). This is outrageous: but
this outrageousness has become the new normal. This all means that 'proprietary
software' is more often than not malware.

** What about security?

Some supporters of proprietary software try to justify the unjustness of it by
claiming it is more secure, as nobody can find exploits as the source code is
unavailable. This, however, is blatantly wrong as security through obscurity was
rejected by security experts as far back as 1851; Alfred Charles Hobbs
demonstrated to the public how state of the art locks can be picked. In response
to concerns that exposing design flaws of the locks could make them more
vulnerable, he said: "Rogues are very keen in their profession, and know already
much more than we can teach them." The same applies for software: Microsoft
Windows, the most popular proprietary operating system, is probably the most
vulnerable to computer viruses and other malware. On the other hand, GNU/Linux
and *BSD distributions, which are mostly free software, are more secure because
of their free and open nature: everyone can inspect the source code of the
kernel, improve it and patch vulnerablities. 

* Sources

1. https://www.gnu.org/proprietary/proprietary.html
2. https://www.gnu.org/philosophy/free-software-even-more-important.html
3. https://en.wikipedia.org/wiki/Security_through_obscurity
4. https://www.theguardian.com/technology/2009/jul/17/amazon-kindle-1984
